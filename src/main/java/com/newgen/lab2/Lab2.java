/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.newgen.lab2;

import java.util.Scanner;

/**
 *
 * @author Phattharaphon
 */
public class Lab2 {
        static char[][] T = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        static int col, row;
        static char player = 'O';
        static int equal = 0;
        


    public static void main(String[] args) {
        System.out.println("Welcome to OX Game");
        System.out.println("Turn O player");
        while (true) {
            showTable();
            inputCR();
            if (check() || checkXY()) {
                showTable();
                System.out.println("Win");
                break;
            } else if (checkDrow()) {
                System.out.println("equal");
                break;
            }
            equal = equal + 1;
            switchPlayer();
        }
    }

    
    public static void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(T[i][j]);
            }
            System.out.println();
        }
    }
    public static void inputCR() {
        Scanner kb = new Scanner(System.in);
        row = kb.nextInt();
        col = kb.nextInt();
        T[row][col] = player;
    }


    public static void switchPlayer() {
        if (player == 'O') {
            System.out.println("Turn X player");
            player = 'X';
        } else {
            System.out.println("Turn O player");
            player = 'O';
        }
    }
    public static boolean checkX() {
        for (int c = 0; c < 3; c++) {
            if (T[row][c] != player) {
                return false;
            }
        }
        return true;
    }
    public static boolean checkY() {
        for (int r = 0; r < 3; r++) {
            if (T[r][col] != player) {
                return false;
            }
        }
        return true;
    }
    public static boolean check() {
        if (checkX()) return true;
        if (checkY()) return true;
        return false;
    }
        public static boolean checkXY() {
        if (T[0][0] == player && T[1][1] == player && T[2][2] == player) {
            return true;
        } else if (T[0][2] == player && T[1][1] == player && T[2][0] == player) {
            return true;
        } else {
            return false;
        }
    }
    public static boolean checkDrow() {
        if (equal == 8) {
            return true;
        }
        return false;
    }

}